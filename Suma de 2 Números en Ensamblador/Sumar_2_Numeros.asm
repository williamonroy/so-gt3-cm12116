;UNIVERSIDAD DE EL SALVADOR
;FACULTAD MULTIDISCIPLINARIA DE OCCIDENTE
;DEPARTAMENTO DE INGENIERIA Y ARQUITECTURA

;Sumar de dos numeros enteros

name 'Suma dos numeros'
include 'emu8086.inc'
org 100h

.data
suma db 2 dub(?)

.code
Sumas proc
    printn " "
    print "Introduce el primer numero ==> "
    call scan_num
    mov suma[0],cl
    printn ""
    print "Introduce el segundo numero ==> "
    call scan_num
    mov suma[1],cl
    printn " "
    xor ax, ax
    add al, suma[0]
    add al, suma[1]
    printn ""
    print "La suma es: "
    call print_num
Sumas endp

exit:
    printn " "
    printn " "
    print "Presiona enter para salir... "
    mov ah, 0
    int 16h
    ret
define_print_string
define_print_num
define_print_num_uns
define_scan_num
end